$fn=100;

eps=0.01;

difference(){

	union(){
		translate([0,0,0+0.6+1.75+7.5+4]) cylinder(h=0.75,r1=0.75,r2=0);
		translate([0,0,0+0.6+1.75+7.5]) cylinder(h=4,r=0.75);
		translate([0,0,0+0.6+1.75]) cylinder(h=7.5,r1=1.2,r2=2);
		translate([0,0,0+0.6+1.75]) cylinder(h=0.75,r1=1.35,r2=1.5);
		translate([0,0,0+0.6]) cylinder(h=1.75,r1=1.7,r2=1.2);
		translate([0,0,0]) cylinder(h=0.6,r=1.15);
	}

	translate([0,0,-eps]) cylinder(h=0+0.6+1.75+7.5-1.5+eps, r=0.65);

}
