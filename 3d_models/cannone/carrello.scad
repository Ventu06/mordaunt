$fn = 100;

h1p = 0.5;
l1p = 0.65;
ns = 4;

h = 10;
l = 30;
s = 4;

d = 5;

hb = 0.2;
lb = 0.5;

rc = 0.75;
drc = 1.5;

lrp = 0.15;
hrp = 0.15;
rr = 4;
rrh = 0.75;
rs = 1.5;


h2 = h-2*drc*rc;
h1 = h1p*h2;
hs = (h2-h1)/(ns-1);
l1 = l*l1p;
ls = l1/ns;

rb = ((h*hb)^2+(l*lb/2)^2)/(2*h*hb);
yb = h*hb-rb;


translate([0,s+d/2,0]) rotate(90,[1,0,0]) lato();
translate([0,-d/2,0]) rotate(90,[1,0,0]) lato();
color([1,0,0]) translate([0,d/2,0]) rotate(90,[1,0,0]) centro();
color([1,0,0]) translate([l*lrp,s+d/2+rs,h*hrp]) rotate(90,[1,0,0]) ruota();
color([1,0,0]) translate([l*(1-lrp),s+d/2+rs,h*hrp]) rotate(90,[1,0,0]) ruota();
color([1,0,0]) translate([l*lrp,-s-d/2,h*hrp]) rotate(90,[1,0,0]) ruota();
color([1,0,0]) translate([l*(1-lrp),-s-d/2,h*hrp]) rotate(90,[1,0,0]) ruota();


module lato(){
	linear_extrude(s) difference(){
		polygon(
			[[0,0]
			,[0*ls,h1]
			,[1*ls,h1+0*hs]
			,[1*ls,h1+1*hs]
			,[2*ls,h1+1*hs]
			,[2*ls,h1+2*hs]
			,[3*ls,h1+2*hs]
			,[3*ls,h1+3*hs]
			,[4*ls,h1+3*hs]
			,[l1+((l-l1)-2*drc*rc)/2,h]
			,[l-((l-l1)-2*drc*rc)/2,h]
			,[l,h2]
			,[l,0]
			]);
		union(){
			translate([l/2,yb]) circle(rb);
			translate([l*lrp,h*hrp]) circle(rrh);
			translate([l*(1-lrp),h*hrp]) circle(rrh);
			translate([l1+(l-l1)/2,h2+rc]) circle(rc);
		}
	}
}

module centro(){
	linear_extrude(d) difference(){
		square([l,h1]);
		union(){
			translate([l/2,yb]) circle(rb);
			translate([l*lrp,h*hrp]) circle(rrh);
			translate([l*(1-lrp),h*hrp]) circle(rrh);
		}
	}
}

module ruota(){
	linear_extrude(rs) difference(){
	circle(rr);
	circle(rrh);
	}

}
