$fn=100;

eps=0.01;

difference(){

	union(){
		translate([0,0,0+0.75+3+14.75+5.75+5.25]) cylinder(h=1,r1=1,r2=0);
		translate([0,0,0+0.75+3+14.75+5.75]) cylinder(h=5.25,r=1);
		translate([0,0,0+0.75+3+14.75]) cylinder(h=1,r1=2.75,r2=3);
		translate([0,0,0+0.75+3+14.75]) cylinder(h=5.75,r=2.5);
		translate([0,0,0+0.75+3]) cylinder(h=14.75,r1=1.9,r2=2.5);
		translate([0,0,0+0.75+3]) cylinder(h=1,r1=2,r2=2.25);
		translate([0,0,0+0.75]) cylinder(h=3,r1=2.5,r2=1.9);
		translate([0,0,0]) cylinder(h=0.75,r=1.7);
	}

	translate([0,0,-eps]) cylinder(h=0+0.75+3+14.75+eps, r=1);

}
