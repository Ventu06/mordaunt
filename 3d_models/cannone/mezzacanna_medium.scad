$fn=100;

eps=0.01;

difference(){

	union(){
		translate([0,0,0+0.75+2+9.5+4]) cylinder(h=0.75,r1=0.75,r2=0);
		translate([0,0,0+0.75+2+9.5]) cylinder(h=4,r=0.75);
		translate([0,0,0+0.75+2]) cylinder(h=9.5,r1=1.3,r2=2);
		translate([0,0,0+0.75+2]) cylinder(h=0.75,r1=1.5,r2=1.7);
		translate([0,0,0+0.75]) cylinder(h=2,r1=1.8,r2=1.3);
		translate([0,0,0]) cylinder(h=0.75,r=1.3);
	}

	translate([0,0,-eps]) cylinder(h=0+0.75+2+9.5-2+eps, r=0.75);

}
