use <dot/arc_path.scad>;
use <dot/ring_extrude.scad>;

/*r1 = 10;*/
/*r2 = 8;*/
r1 = 11;
r2 = 9;
h1 = 2;
h2 = 1;
n=8;
a=7;
k=0.6;

rm=(r1+r2)/2;
dr=(r1-r2)/2;

pts = concat(
	[[-dr,0], [dr,0]],
	[for (p=arc_path(radius = h2, angle = [0, 90])) p+[dr-h2,h1]],
	[for (p=arc_path(radius = h2, angle = [90, 180])) p+[-dr+h2,h1]]
	);

for (i=[1:n]){
	rotate(360/n*i){
		ring_extrude(pts, radius=rm, angle=360/(2*n), scale=k);
		mirror([1,0,0]) ring_extrude(pts, radius=rm, angle=360/(2*n), scale=k);
		rotate(360/(2*n)-a/2) ring_extrude([for (p=pts) p*k*1.5], radius=rm, angle=a);
	}
}
