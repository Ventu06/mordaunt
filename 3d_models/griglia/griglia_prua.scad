s = 3;
d = 4/3;
dd = 1/2;

ny = 20;
nx = 50;


linear_extrude(s){

	import("griglia_prua_bordo.svg");

	intersection(){

		union(){
			for(i=[1:nx]){
				translate([(2*i-1-nx-0.5)*d+dd/2,(-ny-0.5)*d]) square([d-dd,(2*ny+1)*d]);
			}
			for(i=[1:ny]){
				translate([(-nx-0.5)*d,(2*i-1-ny-0.5)*d+dd/2]) square([(2*nx+1)*d,d-dd]);
			}
		}

		import("griglia_prua_interno.svg");

	}

}
