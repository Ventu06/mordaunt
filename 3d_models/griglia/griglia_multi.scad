b = 3;
s = 3;
tz = 0.75;
td = 0.5;
d = 4/3;
dd = 1/2;

/*Griglia 1*/
xf1 = 27.5;
xf2 = 13.5;
y = 22;

/*Griglia 3*/
/*xf1 = 38.5;*/
/*xf2 = 45;*/
/*y = 33;*/

xm = 6;

x1 = xf1+(xm-b)/2;
x2 = xf2+(xm-b)/2;

eps = 0.01;

ny = round((y/d-1)/2);
nx1 = round((x1/(y/(2*ny+1))-1)/2);
nx2 = round((x2/(y/(2*ny+1))-1)/2);

dx1=x1/(2*nx1+1);
dx2=x2/(2*nx2+1);
dy=y/(2*ny+1);


difference(){

	linear_extrude(s){

		for(i=[1:nx1]){
			translate([(2*i-1)*dx1+dd/2,-eps]) square([d-dd,y+2*eps]);
		}

		for(i=[1:nx2]){
			translate([x1+b+(2*i-1)*dx2+dd/2,-eps]) square([d-dd,y+2*eps]);
		}

		for(i=[1:ny]){
			translate([-eps,(2*i-1)*dy+dd/2]) square([x1+2*eps,d-dd]);
			translate([x1+b-eps,(2*i-1)*dy+dd/2]) square([x2+2*eps,d-dd]);
		}

		translate([-b,-b-eps]) square([b,y+2*b+2*eps]);
		translate([x1,-b-eps]) square([b,y+2*b+2*eps]);
		translate([x1+x2+b,-b-eps]) square([b,y+2*b+2*eps]);
		translate([-b-eps,-b]) square([x1+x2+3*b+2*eps,b]);
		translate([-b-eps,y]) square([x1+x2+3*b+2*eps,b]);

	}


	translate([0,0,s-tz]) linear_extrude(tz+eps){

		translate([0,0]) square([td,y]);
		translate([x1-td,0]) square([td,y]);
		translate([x1+b,0]) square([td,y]);
		translate([x1+x2+b-td,0]) square([td,y]);
		translate([-b-eps,0]) square([x1+x2+3*b+2*eps,td]);
		translate([-b-eps,y-td]) square([x1+x2+3*b+2*eps,td]);

	}

}
