b = 3;
s = 3;
tz = 0.75;
td = 0.5;
d = 4/3;
dd = 1/2;

/*Griglia 2*/
/*x = 47.5;*/
/*y = 33;*/

/*Griglia 4*/
x = 39;
y = 33;

eps = 0.01;

ny = round((y/d-1)/2);
nx = round((x/(y/(2*ny+1))-1)/2);

dx=x/(2*nx+1);
dy=y/(2*ny+1);


difference(){

	linear_extrude(s){

		for(i=[1:nx]){
			translate([(2*i-1)*dx+dd/2,-eps]) square([d-dd,y+2*eps]);
		}

		for(i=[1:ny]){
			translate([-eps,(2*i-1)*dy+dd/2]) square([x+2*eps,d-dd]);
		}

		translate([-b,-b-eps]) square([b,y+2*b+2*eps]);
		translate([x,-b-eps]) square([b,y+2*b+2*eps]);
		translate([-b-eps,-b]) square([x+2*b+2*eps,b]);
		translate([-b-eps,y]) square([x+2*b+2*eps,b]);

	}


	translate([0,0,s-tz]) linear_extrude(tz+eps){

		translate([0,0]) square([td,y]);
		translate([x-td,0]) square([td,y]);
		translate([-b-eps,0]) square([x+2*b+2*eps,td]);
		translate([-b-eps,y-td]) square([x+2*b+2*eps,td]);

	}

}
